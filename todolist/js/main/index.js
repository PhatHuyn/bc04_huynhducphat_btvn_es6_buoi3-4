import { renderDSTask1, renderDSTask2, renderSapXepAZ } from "../Controller/Task.controller.js";

const BASE_URL = "https://62db6ce8e56f6d82a7728883.mockapi.io";

function dsTask() {
    axios({
    url: `${BASE_URL}/task`,
    method: "GET",
    })
    .then(function(res){
        // console.log(res);
        renderDSTask1(res.data);
    })
    .catch(function(err){
        console.log(err);
    });
}
function dsTask2() {
    axios({
    url: `${BASE_URL}/task2`,
    method: "GET",
    })
    .then(function(res){
        // console.log(res);
        renderDSTask2(res.data);
    })
    .catch(function(err){
        console.log(err);
    });
}
dsTask();
dsTask2();
function ThemTask(){
    let nd = document.getElementById("newTask").value;
    let newTask = {
        id: "9999",
        noidung: nd,    
    }    
    axios({
        url: `${BASE_URL}/task`,
        method: "POST",
        data: newTask,
        })
        .then(function(res){
            console.log(res);
            document.getElementById("newTask").value = "";
            dsTask();
        })
        .catch(function(err){
            console.log(err);
        });

}
function XoaTask(id){
    axios({
        url: `${BASE_URL}/task/${id}`,
        method: "DELETE",
        })
        .then(function(res){
            console.log(res);
            dsTask();
        })
        .catch(function(err){
            console.log(err);
        });
}
function XoaTask2(id){
    axios({
        url: `${BASE_URL}/task2/${id}`,
        method: "DELETE",
        })
        .then(function(res){
            console.log(res);
            dsTask2();
        })
        .catch(function(err){
            console.log(err);
        });
}
function CheckTask(id){
    // lấy dữ liệu về từ task1 
    axios({
        url: `${BASE_URL}/task/${id}`,
        method: "GET",
        })
        .then(function(res){
            let nd = res.data;
            // thêm dữ liệu vào task2
            axios({
                url: `${BASE_URL}/task2`,
                method: "POST",
                data: nd,
                })
                .then(function(res){
                    console.log(res);
                    dsTask2();
                })
                .catch(function(err){
                    console.log(err);
                });
        })
        .catch(function(err){
            console.log(err);
        });
        // xoá dữ liệu axios từ task1
        axios({
            url: `${BASE_URL}/task/${id}`,
            method: "DELETE",
            })
            .then(function(res){
                console.log(res);
                dsTask();
            })
            .catch(function(err){
                console.log(err);
            });
}
function CheckTask2(id){
    // lấy dữ liệu về từ task2 
    axios({
        url: `${BASE_URL}/task2/${id}`,
        method: "GET",
        })
        .then(function(res){
            let nd = res.data;
            // thêm dữ liệu vào task1
            axios({
                url: `${BASE_URL}/task`,
                method: "POST",
                data: nd,
                })
                .then(function(res){
                    console.log(res);
                    dsTask();
                })
                .catch(function(err){
                    console.log(err);
                });
        })
        .catch(function(err){
            console.log(err);
        });
        // xoá dữ liệu axios từ task2
        axios({
            url: `${BASE_URL}/task2/${id}`,
            method: "DELETE",
            })
            .then(function(res){
                console.log(res);
                dsTask2();
            })
            .catch(function(err){
                console.log(err);
            });
}
function SapXepTheoAZ(){
    let dssxAZ = [];
    // lấy nội dung để so sánh
    axios({
        url: `${BASE_URL}/task`,
        method: "GET",
        })
        .then(function(res){
            let nd = res.data;
            // sắp xếp theo a-z
            nd.sort((x,y) => {
                let a = x.noidung.toUpperCase(),
                    b = y.noidung.toUpperCase();
                return a == b ? 0 : a > b ? 1 : -1;
            });
            // console.log("nd2", nd);
            dssxAZ.push(nd);
            // console.log('dssxAZ: ', dssxAZ);
            renderSapXepAZ(dssxAZ);
            // console.log(1);
        })
        .catch(function(err){
            console.log(err);
        });
}
function SapXepTheoZA(){
    let dssxZA = [];
    // lấy nội dung để so sánh
    axios({
        url: `${BASE_URL}/task`,
        method: "GET",
        })
        .then(function(res){
            let nd = res.data;
            // sắp xếp theo z-a
            nd.sort((x,y) => {
                let a = x.noidung.toUpperCase(),
                    b = y.noidung.toUpperCase();
                return a == b ? 0 : b > a ? 1 : -1;
            });
            // console.log("nd2", nd);
            dssxZA.push(nd);
            // console.log('dssxAZ: ', dssxZA);
            renderSapXepAZ(dssxZA);
            // console.log(1);
        })
        .catch(function(err){
            console.log(err);
        });
}
window.SapXepTheoZA = SapXepTheoZA;
window.SapXepTheoAZ = SapXepTheoAZ;
window.CheckTask2 = CheckTask2;
window.CheckTask = CheckTask;
window.XoaTask = XoaTask;
window.XoaTask2 = XoaTask2;
window.ThemTask = ThemTask;