
export function renderDSTask1(dstask){
    let contentHTML = "";   
    dstask.forEach((item) => {
        let content = `
        <div class="TodoTask mb-2">
            <span class="spantask">${item.noidung}</span>
            <div class="TodoTaskitem">
            <a id="rac" onclick="XoaTask('${item.id}')"><i class="fa-solid fa-trash-can"></i></a>
            <a id="check" value="${item.noidung}" onclick="CheckTask('${item.id}')"><i class="fa-solid fa-circle-check"></i></a>
            </div>
        </div>
        `;
        contentHTML += content;
    });
    document.getElementById("todo").innerHTML = contentHTML;
}

export function renderDSTask2(dstask){
    let contentHTML = "";   
    dstask.forEach((item) => {
        let content = `
        <div class="TodoTask mb-2">
            <span class="spantask">${item.noidung}</span>
            <div class="TodoTaskitem">
            <a id="rac" onclick="XoaTask2('${item.id}')"><i class="fa-solid fa-trash-can"></i></a>
            <a id="check2" onclick="CheckTask2('${item.id}')"><i class="fa-solid fa-circle-check"></i></a>
            </div>
        </div>
        `;
        contentHTML += content;
    });
    document.getElementById("completed").innerHTML = contentHTML;
}


export function renderSapXepAZ(dssx){
    let contentHTML = "";   
    for(let i = 0; i<dssx.length;i++){
        let nd = dssx[i];
        for(let j = 0;j<nd.length;j++){
            let ndc = nd[j];
            // console.log('ndc: ', ndc);
            let content = `
            <div class="TodoTask mb-2">
                <span class="spantask">${ndc.noidung}</span>
            <div class="TodoTaskitem">
                <a id="rac" onclick="XoaTask('${ndc.id}')"><i class="fa-solid fa-trash-can"></i></a>
                <a id="check" value="${ndc.noidung}" onclick="CheckTask('${ndc.id}')"><i class="fa-solid fa-circle-check"></i></a>
            </div>
            </div>`;
            contentHTML += content;
        }
    }
    document.getElementById("todo").innerHTML = contentHTML;
}

